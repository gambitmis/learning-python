from flask import Flask,request

stores = [
    {
        "name": "My Store",
        "items": [
            {
                "name": "Chair",
                "price": 15.99
            }
        ]
    },
    {
        "name": "Nike",
        "items": [
            {
                "name": "Air",
                "price": 30.99
            }
        ]
    },
]

app = Flask(__name__)

@app.get("/store")
def get_store():
    return {"stores": stores}

@app.post("/store")
def create_store():
    request_data = request.get_json()
    new_store = {"name": request_data["name"], "items":[]}
    stores.append(new_store)
    return new_store, 201

@app.post("/store/<string:name>/item")
def create_item(name):
    request_data = request.get_json()
    for store in stores:
        if store["name"] == name:
            new_item = {"name": request_data["name"],"price": request_data["price"]}
            store["items"].append(new_item)
            return new_item
    return {"message":"Store not found"}, 404

@app.get("/store/<string:name>")
def get_stores(name):
    for store in stores:
        if store["name"] == name:
            return store
    return {"message":"Store not found"}, 404


@app.get("/store/<string:name>/item")
def get_item_in_store(name):
    for store in stores:
        if store["name"] == name:
            return {"items": store["items"]}
    return {"message":"Store not found"}, 404


if __name__ == '__main__':
    app.run(host="0.0.0.0",port=5000,debug=True)