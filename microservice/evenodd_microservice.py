from flask import Flask, jsonify
import requests

app = Flask(__name__)

random_microservice_url = "http://127.0.0.1:5001/gen"

# Calling the random num gen microservice
def call_random_microservice():
    response = requests.get(random_microservice_url)
    return response.json().get("random_number")

@app.route("/check", methods=['GET'])
def check_even_odd():
    random_number = call_random_microservice()
    res = "even" if random_number % 2 == 0 else "odd"
    return jsonify({"random_number": random_number, "result": res})

if __name__ == '__main__':
    app.run(host='0.0.0.0',port=5000,debug=True)