from flask import Flask, jsonify
import random

app = Flask(__name__)

@app.route("/gen", methods=['GET'])
def gen_random_num():
    random_num = random.randint(1,1000)
    return jsonify({"random_number": random_num})

if __name__ == '__main__':
    app.run(port=5001,host='0.0.0.0',debug=True)